const express = require("express");
const dashboardRouter = express.Router();
const { UserGames, UserGameBiodata, UserGameHistories } = require("../models");

UserGames.hasOne(UserGameBiodata, { foreignKey: "user_id", as: "UserGameBiodata" });
UserGames.hasOne(UserGameHistories, { foreignKey: "user_id", as: "UserGameHistories" });

dashboardRouter.get("/", (req, res) => {
  UserGames.findAll({
    order: [["id", "ASC"]],
    include: ["UserGameBiodata", "UserGameHistories"],
  }).then((users) => {
    res.render("dashboard", { users });
    console.log(users);
  });
});

dashboardRouter.get("/create", (req, res) => {
  res.render("create");
});

dashboardRouter.get("/edit/:id", async (req, res) => {
  const user = await UserGames.findOne({
    where: { id: req.params.id },
  });
  const biodata = await UserGameBiodata.findOne({
    where: { user_id: req.params.id },
  });
  res.render("edit", { user, biodata });
  console.log(user, biodata);
});

module.exports = dashboardRouter;
