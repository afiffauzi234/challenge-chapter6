const express = require("express");
const methodOverride = require("method-override");
const app = express();
const port = 3000;
const router = require("./routers/index");
const login = require("./controllers/UserManagement/login");
const dashboard = require("./controllers/UserManagement/dashboard");

app.set("view engine", "ejs");

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(methodOverride("_method"));

app.use(router);
app.use(login);
app.use(dashboard);

app.use("/", (req, res) => {
  res.status(404);
  res.json({
    status: "404",
    errors: "page not found",
  });
});

app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
