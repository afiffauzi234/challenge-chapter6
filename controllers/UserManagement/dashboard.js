const express = require("express");
const { user } = require("pg/lib/defaults");
const { sequelize } = require("../../models");
const dashboard = express.Router();
const { UserGames, UserGameBiodata, UserGameHistories } = require("../../models");

UserGames.hasOne(UserGameBiodata, { foreignKey: "user_id", as: "usergamebiodata" });
UserGames.hasOne(UserGameHistories, { foreignKey: "user_id", as: "usergameistories" });

dashboard.post("/dashboard/create", (req, res) => {
  const { username, password, fullname, age } = req.body;
  UserGames.create({
    username,
    password,
  })
    .then((user) => {
      UserGameBiodata.create({
        fullname,
        age,
        user_id: user.dataValues.id,
      });
      UserGameHistories.create({
        score: 0,
        user_id: user.dataValues.id,
      });
    })
    .then((users) => {
      res.redirect("/dashboard");
      console.log(users);
    });
});

dashboard.post("/dashboard/edit/:id", (req, res) => {
  const { username, fullname, age } = req.body;
  UserGames.update(
    {
      username,
    },
    {
      where: {
        id: req.params.id,
      },
    }
  )
    .then(() => {
      UserGameBiodata.update(
        {
          fullname,
          age,
        },
        {
          where: {
            user_id: req.params.id,
          },
        }
      );
    })
    .then((users) => {
      res.redirect("/dashboard");
      console.log(users);
    });
});

dashboard.delete("/dashboard/:id", (req, res) => {
  UserGames.destroy({
    where: {
      id: req.params.id,
    },
  })
    .then(() => {
      UserGameBiodata.destroy({
        where: {
          user_id: req.params.id,
        },
      });
    })
    .then(() => {
      UserGameHistories.destroy({
        where: {
          user_id: req.params.id,
        },
      });
    })
    .then((users) => {
      res.redirect("/dashboard");
      console.log(users);
    });
});

module.exports = dashboard;
