const fs = require("fs");
const express = require("express");
const login = express.Router();
const database = require("../../database/user.json");

login.use(express.json());
login.use(express.urlencoded({ extended: false }));

login.post("/login", (req, res) => {
  let request = req.body;
  let userData = database;
  for (let i = 0; i < userData.length; i++) {
    const element = userData[i];
    if (request.username === element.username && request.password === element.password) {
      res.status(200);
      res.render("dashboard");
      res.redirect("/dashboard");
    } else {
      res.status(401);
      res.send("Invalid username or password!");
    }
  }
});

module.exports = login;
